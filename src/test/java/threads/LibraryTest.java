package threads;

import org.junit.Test;
import static org.junit.Assert.*;


/*
 * This Java source file was auto generated by running 'gradle init --type java-library'
 * by 'Roman_Kuznetcov' at '4/22/16 5:33 PM' with Gradle 2.6
 *
 * @author Roman_Kuznetcov, @date 4/22/16 5:33 PM
 */
public class LibraryTest {
    @Test public void testSomeLibraryMethod() {
        App classUnderTest = new App();
        assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());
    }
}
