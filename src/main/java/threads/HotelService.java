package threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HotelService {


  public void startWork() {

    ExecutorService producers = Executors.newFixedThreadPool(3);
    ExecutorService consumers = Executors.newFixedThreadPool(6);
    BikeQueue oneQueue = new BikeQueue();

    List<Producer> producersList = new ArrayList<>();
    producersList.add(new Producer("without rats", "SweetHole", oneQueue));
    producersList.add(new Producer("free cooqies", "DaddyVader", oneQueue));
    producersList.add(new Producer("welcome", "Hell", oneQueue));

    Consumer consumer = new Consumer(oneQueue);


    for (int i = 0; i < 3; i++) {
      producers.execute(producersList.get(i % 3));
    }

    for (int i = 0; i < 6; i++) {
      consumers.execute(consumer);
    }

    producers.shutdown();
    consumers.shutdown();

  }

}
