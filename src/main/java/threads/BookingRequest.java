package threads;

import java.time.LocalDate;

public class BookingRequest {

  String ad;
  LocalDate date;
  String hotelname;
  Integer rooms;
  Integer capacity;
  boolean flag;

  public BookingRequest(String ad, LocalDate date, String hotelname, Integer rooms,
      Integer capacity) {
    super();
    this.ad = ad;
    this.date = date;
    this.hotelname = hotelname;
    this.rooms = rooms;
    this.capacity = capacity;
  }

}
