package threads;

import java.util.ArrayList;
import java.util.List;

public class BikeQueue {

  private List<BookingRequest> requestList = new ArrayList<>();

  public synchronized void addRequest(BookingRequest request, Producer whoDare) {

    requestList.add(request);
    System.out.println("Producer " + Thread.currentThread() +" sent "+request.ad);
    notifyAll();
  }


  public synchronized BookingRequest getLastRequest(Consumer whoDare) {

    while (isEmptyQueue()){
      try {
        wait();
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    BookingRequest request = null;
    request = requestList.get(requestList.size() - 1);
    requestList.remove(requestList.size() - 1);
    return request;
  }

  public boolean isEmptyQueue() {
    return requestList.isEmpty();
  }
}
