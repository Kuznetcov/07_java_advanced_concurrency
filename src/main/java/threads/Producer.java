package threads;

import java.time.LocalDate;
import java.util.Random;

public class Producer implements Runnable {

  private static Integer produced = 0;

  private String hotelname;
  private String ad;
  private BikeQueue queue;

  public Producer(String ad, String hotelname, BikeQueue queue) {
    this.hotelname = hotelname;
    this.ad = ad;
    this.queue = queue;
  }

  @Override
  public void run() {
    Thread thisTread = Thread.currentThread();
    System.out.println("Запустили поток: " + thisTread);
    Random rnd = new Random();
    long week = 1l;

    while (Producer.getProduced() < 15) {

      BookingRequest newOffer = new BookingRequest(this.ad, LocalDate.now().plusWeeks(week++),
          this.hotelname, rnd.nextInt(5) + 1, rnd.nextInt(10) + 1);
      queue.addRequest(newOffer, this);
      Producer.incProduced();

      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }


    System.out.println(thisTread + "- этот поток создания реквестов отработал");
  }


  public static synchronized Integer getProduced() {
    return produced;
  }

  public static synchronized void incProduced() {
    Producer.produced++;
  }
}
