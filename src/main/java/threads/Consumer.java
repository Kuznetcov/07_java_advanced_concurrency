package threads;

public class Consumer implements Runnable {

  private BikeQueue queue;
  
  public Consumer(BikeQueue queue){
    
    this.queue = queue;
    
  }
  
  
  @Override
  public void run() {
    Thread thisTread = Thread.currentThread();
    System.out.println("Запустили поток: "+ thisTread);
    do {

      BookingRequest myOffer = takeOffer();
      System.out.println("Consumer "+ thisTread+ " received " + myOffer.ad);
      try {
        Thread.sleep(5000);
        System.out.println(thisTread+ " processed " + myOffer.ad);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    } while (!queue.isEmptyQueue());

    System.out.println(thisTread+"- этот поток обработки реквестов отработал");
  }


  public BookingRequest takeOffer() {

    return queue.getLastRequest(this);
  
  }
  
}
